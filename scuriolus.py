import os
from pathlib import Path

import httpx
import minicli
from roll import Roll, HttpError
from roll.extensions import simple_server, traceback, options, cors

app = Roll()
traceback(app)
options(app)
cors(app, methods="*", headers=["*", "Authorization", "Content-Type"])


def validate_path(view):
    def validator(request, response, path):
        filepath = (app["ROOT"] / path).resolve()
        if app["ROOT"].resolve() not in filepath.parents:
            raise HttpError(400, f"Invalid path {path}")
        return view(request, response, path=filepath)

    return validator


def auth(view):
    async def validator(request, response, *args, **kwargs):
        headers = {"Authorization": request.headers.get("AUTHORIZATION", "")}
        try:
            async with httpx.AsyncClient() as client:
                remote = await client.get(app["AUTH_URL"], headers=headers)
        except OSError:
            raise HttpError(502, "Cannot connect to remote auth server.")
        if remote.is_error:
            raise HttpError(401, f"Remote status: {remote.status_code}")
        return await view(request, response, *args, **kwargs)

    return validator


@app.route("/{path:path}", methods=["PUT"])
@validate_path
@auth
async def put(request, response, path):
    path.parent.mkdir(parents=True, exist_ok=True)
    path.write_bytes(request.body)
    response.status = 204


@app.route("/{path:path}", methods=["GET"])
@validate_path
async def get(request, response, path):
    if not path.exists():
        raise HttpError(404, str(path))
    if path.is_dir():
        response.json = [
            f"/{p.relative_to(app['ROOT'].resolve())}"
            for p in path.glob("**/*")
            if p.is_file()
        ]
    else:  # Never do that in production!
        response.body = path.read_bytes()


@app.listen("startup")
async def on_startup():
    app["ROOT"] = Path(os.environ.get("SCURIOLUS_ROOT", "storage"))
    app["AUTH_URL"] = os.environ.get(
        "SCURIOLUS_AUTH_URL", "http://localhost:9000/api/v1/users/me"
    )
    app["ROOT"].mkdir(parents=True, exist_ok=True)


@minicli.cli
def serve(reload=False):
    """Run a web server (for development only)."""
    if reload:
        import hupper

        hupper.start_reloader("scuriolus.serve")
    try:
        port = int(os.environ.get("PORT"))
    except (ValueError, TypeError):
        port = 4321
    host = os.environ.get("HOST", "127.0.0.1")
    simple_server(app, host=host, port=port)


def main():
    minicli.run()
