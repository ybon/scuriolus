# Scuriolus

Hysterically simple file storage API.

## Install

In a venv:

    python setup.py develop

Set SCURIOLUS_AUTH_URL env var, eg.:

    export SCURIOLUS_AUTH_URL="https://peertube.space/api/v1/users/me"


## Usage

    scuriolus serve

Test it with httpie:

    http PUT :4321/test/pouet.pdf @path/to/some.pdf Authorization:"Bearer <the access token provided by peertube>"
    http :4321/test


## Deployment

System packages:

- `python3.7`
- `python3.7-dev`
- `python3.7-venv`
- `nginx`
- `gcc`

Python packages:

- `git+https://framagit.org/ybon/scuriolus`
- `gunicorn`
- `uvloop`

Env vars:

- `SCURIOLUS_ROOT=` where to store files
- `SCURIOLUS_AUTH_URL=` the OAuth provider

Run gunicorn:

    gunicorn scuriolus:app --worker-class roll.worker.Worker --workers 4 --bind unix:/path/to.sock

Nginx example configuration:

```nginx
upstream scuriolus {
    server unix:///path/to.sock;
}

server {
    listen 80;
    server_name mydomain;

    charset     utf-8;
    client_max_body_size 3M;

    root /path/to/SCURIOLUS_ROOT;

    try_files $uri @forward;

    location @forward {
        proxy_pass http://scuriolus/;
        proxy_set_header Host            $host;
        proxy_set_header X-Forwarded-For $remote_addr;
    }

}

```
