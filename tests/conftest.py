import os

import pytest


from scuriolus import app as myapp


def pytest_configure(config):
    os.environ["SCURIOLUS_ROOT"] = "tests/storage"


@pytest.fixture
def app():
    return myapp
